/*
import ir.asreDaneshAfzar.assesment.entity.ContactEntity;
import ir.asreDaneshAfzar.assesment.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

*/
/**
 * @Author: s.shakeri
 * at 5/24/2021
 **//*


@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@EnableJpaRepositories(basePackageClasses = ContactRepository.class)
@EntityScan(basePackageClasses = ContactEntity.class)
public class ContactRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ContactRepository contactRepository;   //TODO : what is the issue with repositories in this project!!??

    @Test
    public void should_find_no_contacts_if_repository_is_empty() {
        Iterable<ContactEntity> contactEntities = contactRepository.findAll();

        assertThat(contactEntities).isEmpty();
    }

    @Test
    public void should_store_a_contact() {
        ContactEntity contactEntity = contactRepository.save(new ContactEntity("sobhan", "09382624369",
                "sobhan.shakery@gmail.com", "aminSSD", "26241996", 0));

        assertThat(contactEntity).hasFieldOrPropertyWithValue("name", "sobhan");
        assertThat(contactEntity).hasFieldOrPropertyWithValue("phoneNumber", "09382624369");
        assertThat(contactEntity).hasFieldOrPropertyWithValue("email", "sobhan.shakery@gmail.com");
        assertThat(contactEntity).hasFieldOrPropertyWithValue("organization", "aminSSD");
        assertThat(contactEntity).hasFieldOrPropertyWithValue("githubAccountName", "26241996");
        assertThat(contactEntity).hasFieldOrPropertyWithValue("githubRepositoriesCount", 0);
    }

    @Test
    public void should_find_by_given_values() {
        List<ContactEntity> contactEntities = contactRepository.findByOptional("sobhan", "", "26241996",
                "09382624369", "");
    }
}
*/
