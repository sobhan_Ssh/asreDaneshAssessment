package ir.asreDaneshAfzar.assesment.service;

import ir.asreDaneshAfzar.assesment.dto.contact.ContactDto;
import ir.asreDaneshAfzar.assesment.entity.ContactEntity;
import ir.asreDaneshAfzar.assesment.exception.NoRepositoryFoundOnGithubWithGivenGithubNameException;
import ir.asreDaneshAfzar.assesment.exception.NoSearchResultException;
import ir.asreDaneshAfzar.assesment.exception.NullRequiredFieldException;
import ir.asreDaneshAfzar.assesment.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 5/20/2021
 **/

@Service
@PropertySource(value = "classpath:messages.properties", encoding = "UTF-8")
@ComponentScan("ir.asreDaneshAfzar.assesment")
public class ContactService {


    @Autowired
    private Environment env;
    @Autowired
    private GithubService githubService;
    @Autowired
    private ContactRepository contactRepository;

    public ContactDto addContact(ContactDto contactDto) throws Exception {
        try {
            checkRequiredFields(contactDto);
            Integer repositoryCount;
            repositoryCount = githubService.getGithubRepositoryCountByGithubName(contactDto.getGithubAccountName());
            if (Objects.nonNull(repositoryCount)) {
                ContactEntity contactEntity = new ContactEntity(contactDto.getName(), contactDto.getPhoneNumber(), contactDto.getEmail(),
                        contactDto.getOrganization(), contactDto.getGithubAccountName(), repositoryCount);
              return mapContactEntityToContactDto(contactRepository.save(contactEntity));
            } else {
                throw new NoRepositoryFoundOnGithubWithGivenGithubNameException(env.getProperty("1102"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<ContactDto> searchContact(ContactDto contactDto) throws Exception {
        try {
            List<ContactEntity> contactEntities = contactRepository.findByOptional(contactDto.getName(), contactDto.getEmail(), contactDto.getGithubAccountName(),
                    contactDto.getPhoneNumber(), contactDto.getOrganization());
            if (Objects.nonNull(contactEntities))
                return mapContactEntityListToContactDtoList(contactEntities);
            else
                throw new NoSearchResultException(env.getProperty("1101"));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void checkRequiredFields(ContactDto contactDto) throws Exception {
        if (Objects.isNull(contactDto.getGithubAccountName()) || Objects.isNull(contactDto.getOrganization()) || Objects.isNull(contactDto.getEmail()) ||
                Objects.isNull(contactDto.getName()) || Objects.isNull(contactDto.getPhoneNumber()))
            throw new NullRequiredFieldException(env.getProperty("1103"));
    }

    private ContactDto mapContactEntityToContactDto(ContactEntity contactEntity) {
        return new ContactDto(contactEntity.getId(), contactEntity.getName(), contactEntity.getPhoneNumber(),
                contactEntity.getEmail(), contactEntity.getOrganization(), contactEntity.getGithubAccountName(), contactEntity.getGithubRepositoriesCount());
    }

    private List<ContactDto> mapContactEntityListToContactDtoList(List<ContactEntity> contactEntities) {
        List<ContactDto> contactDtos = new ArrayList<>();
        for (ContactEntity contactEntity : contactEntities)
        contactDtos.add(new ContactDto(contactEntity.getId(), contactEntity.getName(), contactEntity.getPhoneNumber(),
                contactEntity.getEmail(), contactEntity.getOrganization(), contactEntity.getGithubAccountName(), contactEntity.getGithubRepositoriesCount()));

        return contactDtos;
    }
}
