package ir.asreDaneshAfzar.assesment.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.asreDaneshAfzar.assesment.dto.github.GithubAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 5/21/2021
 **/

@Service
public class GithubService {

    private final String GITHUB_USER_URL = "https://api.github.com/users/";

    @Autowired
    private RestExecutorService restExecutorService;

    public Integer getGithubRepositoryCountByGithubName(String githubAccountName) throws Exception {
        GithubAccountDto githubAccountDto = null;
        try {
            githubAccountDto = getAccountByName(githubAccountName);
            if (Objects.nonNull(githubAccountDto.getPublicRepos()))
                return githubAccountDto.getPublicRepos();
            else return null;
        } catch (Exception e) {
            throw e;
        }
    }

    private GithubAccountDto getAccountByName(String accountName) throws Exception{
        GithubAccountDto accountDto = new GithubAccountDto();
        try {
            String response;
            response = restExecutorService.performGetCall(GITHUB_USER_URL + accountName);
            if (Objects.nonNull(response)) {
                ObjectMapper mapper = new ObjectMapper();
                accountDto = mapper.readValue(response, GithubAccountDto.class);
            }
        } catch (Exception e) {
            throw e;
        }
        return accountDto;
    }
}
