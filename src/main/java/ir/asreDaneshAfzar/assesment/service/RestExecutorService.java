package ir.asreDaneshAfzar.assesment.service;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 5/23/2021
 **/

@Service
public class RestExecutorService {

    public String performGetCall(String requestURL) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(requestURL)
                .method("GET", null)
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/vnd.github.v3+json")
                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0")
                .build();
        try {
            return client.newCall(request).execute().body().string();
        } catch (Exception e) {
            throw e;
        }
    }
}
