package ir.asreDaneshAfzar.assesment.controller;

import ir.asreDaneshAfzar.assesment.dto.ErrorMessage;
import ir.asreDaneshAfzar.assesment.exception.NoRepositoryFoundOnGithubWithGivenGithubNameException;
import ir.asreDaneshAfzar.assesment.exception.NullRequiredFieldException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

/**
 * @Author: s.shakeri
 * at 5/24/2021
 **/

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(NullRequiredFieldException.class)
    @ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY)
    public ErrorMessage nullRequiredFieldException(NullRequiredFieldException ex, WebRequest request) {
        return new ErrorMessage(
                HttpStatus.FAILED_DEPENDENCY.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));
    }

    @ExceptionHandler(NoRepositoryFoundOnGithubWithGivenGithubNameException.class)
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    public ErrorMessage noRepositoryFoundOnGithubWithGivenGithubNameException(NoRepositoryFoundOnGithubWithGivenGithubNameException ex, WebRequest request) {
        return new ErrorMessage(
                HttpStatus.NOT_ACCEPTABLE.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage globalExceptionHandler(Exception ex, WebRequest request) {
        return new ErrorMessage(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));
    }
}


