package ir.asreDaneshAfzar.assesment.controller;

import ir.asreDaneshAfzar.assesment.dto.contact.ContactDto;
import ir.asreDaneshAfzar.assesment.entity.ContactEntity;
import ir.asreDaneshAfzar.assesment.exception.NoRepositoryFoundOnGithubWithGivenGithubNameException;
import ir.asreDaneshAfzar.assesment.exception.NoSearchResultException;
import ir.asreDaneshAfzar.assesment.exception.NullRequiredFieldException;
import ir.asreDaneshAfzar.assesment.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 5/19/2021
 **/

@RestController
@RequestMapping(path = "/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @PutMapping(path = "/add-contact")
    public ResponseEntity<Object> addContact(@RequestBody ContactDto contactDto) {
        try {
            ContactDto contactResponse = contactService.addContact(contactDto);
            if (Objects.nonNull(contactResponse))
                return new ResponseEntity<>(contactResponse, HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.CONFLICT);

        } catch (Exception e) {
            if (e instanceof NullRequiredFieldException)
                return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
            else if (e instanceof NoRepositoryFoundOnGithubWithGivenGithubNameException)
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
            else {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }
    }

    @PostMapping(path = "/search")
    public ResponseEntity<Object> searchContact(@RequestBody ContactDto contactDto) {
        try {
            return new ResponseEntity<Object>(contactService.searchContact(contactDto), HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof NoSearchResultException)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            else
                return new ResponseEntity<Object>(HttpStatus.CONFLICT);
        }
    }

//    @GetMapping(path = "/greeting")
//    public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
//        return "hello word" + name;
//    }
}
