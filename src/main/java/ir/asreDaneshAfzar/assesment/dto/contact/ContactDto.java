package ir.asreDaneshAfzar.assesment.dto.contact;


/**
 * @Author: s.shakeri
 * at 5/19/2021
 **/

public class ContactDto {

    private Long id;

    private String name;

    private String phoneNumber;

    private String email;

    private String organization;

    private String githubAccountName;

    private Integer githubRepositoriesCount;

    public ContactDto(Long id, String name, String phoneNumber, String email, String organization, String githubAccountName, Integer githubRepositoriesCount) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.organization = organization;
        this.githubAccountName = githubAccountName;
        this.githubRepositoriesCount = githubRepositoriesCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithubAccountName() {
        return githubAccountName;
    }

    public void setGithubAccountName(String githubAccountName) {
        this.githubAccountName = githubAccountName;
    }

    public Integer getGithubRepositoriesCount() {
        return githubRepositoriesCount;
    }

    public void setGithubRepositoriesCount(Integer githubRepositoriesCount) {
        this.githubRepositoriesCount = githubRepositoriesCount;
    }
}
