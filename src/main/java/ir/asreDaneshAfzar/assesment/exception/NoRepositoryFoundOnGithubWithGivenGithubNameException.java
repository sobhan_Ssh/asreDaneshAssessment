package ir.asreDaneshAfzar.assesment.exception;

/**
 * @Author: s.shakeri
 * at 5/23/2021
 **/

public class NoRepositoryFoundOnGithubWithGivenGithubNameException extends BaseException {

    public NoRepositoryFoundOnGithubWithGivenGithubNameException(String message) {
        super(message);
    }

    public NoRepositoryFoundOnGithubWithGivenGithubNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
