package ir.asreDaneshAfzar.assesment.exception;

/**
 * @Author: s.shakeri
 * at 5/22/2021
 **/

public class BaseException extends Exception {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
