package ir.asreDaneshAfzar.assesment.exception;

/**
 * @Author: s.shakeri
 * at 5/24/2021
 **/

public class NoSearchResultException extends BaseException{

    public NoSearchResultException(String message) {
        super(message);
    }

    public NoSearchResultException(String message, Throwable cause) {
        super(message, cause);
    }
}
