package ir.asreDaneshAfzar.assesment.exception;

/**
 * @Author: s.shakeri
 * at 5/20/2021
 **/

public class NullRequiredFieldException extends BaseException{

    public NullRequiredFieldException(String message) {
        super(message);
    }

    public NullRequiredFieldException(String message, Throwable cause) {
        super(message, cause);
    }
}
