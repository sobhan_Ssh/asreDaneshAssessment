package ir.asreDaneshAfzar.assesment.repository;

import ir.asreDaneshAfzar.assesment.entity.ContactEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: s.shakeri
 * at 5/20/2021
 **/

@Repository
public interface ContactRepository extends CrudRepository<ContactEntity, Long> {

    @Query("select  c from ContactEntity  c where (:name is null or c.name=:name)  and  (:email is null or c.email=:email) " +
            "and  (:accountName is null or c.githubAccountName=:accountName) and  (:phoneNum is null or c.phoneNumber=:phoneNum) " +
            "and  (:organization is null or c.organization=:organization)")
    List<ContactEntity> findByOptional(String name, String email, String accountName, String phoneNum, String organization);
}
