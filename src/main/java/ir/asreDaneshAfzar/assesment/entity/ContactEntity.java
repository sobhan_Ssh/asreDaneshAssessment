package ir.asreDaneshAfzar.assesment.entity;

import javax.persistence.*;

/**
 * @Author: s.shakeri
 * at 5/20/2021
 **/

@Entity
@Table(name = "ASSESSMENT_CONTACT")
public class ContactEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "PHONE_NUMBER", nullable = false, unique = true)
    private String phoneNumber;

    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @Column(name = "ORGANIZATION", nullable = false)
    private String organization;

    @Column(name = "GITHUB_ACCOUNT_NAME", nullable = false, unique = true)
    private String githubAccountName;

    @Column(name = "GITHUB_REPOSITORIES_COUNT", nullable = false)
    private int githubRepositoriesCount;

    public ContactEntity() {
    }

    public ContactEntity(String name, String phoneNumber, String email, String organization, String githubAccountName, int githubRepositoriesCount) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.organization = organization;
        this.githubAccountName = githubAccountName;
        this.githubRepositoriesCount = githubRepositoriesCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithubAccountName() {
        return githubAccountName;
    }

    public void setGithubAccountName(String githubAccountName) {
        this.githubAccountName = githubAccountName;
    }

    public int getGithubRepositoriesCount() {
        return githubRepositoriesCount;
    }

    public void setGithubRepositoriesCount(int githubRepositoriesCount) {
        this.githubRepositoriesCount = githubRepositoriesCount;
    }
}
