**This project has been implemented with :**

- JAVA 8
- Spring-boot
- Spring-data
- Hibernate
- Maven



**also has some sample REST web service calls to Github API for getting the counts of public repositories of a specific user that has been implemented with :**

- OkHttp


for running :

1. JAVA/Maven should be config on system.
1. clone/copy it to your local system.
1. open in IDE, Intellij Idea is preferd.
1. in "application.properties" file change the value that is assigned to "spring.datasource.url" , change the path for creating H2 database file in your local system.
1. do a "clean-install" operation with Maven.
1. generated .jar file is created in /target folder that can be run with below command in your CMD:

_JAVA -jar ContactWithGithubID-1.0-SNAPSHOT.jar_




